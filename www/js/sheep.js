/**
 * @license WTFPL
 * @file
 */

/**
 * sheep.
 *
 * @constructor
 */
var Sheep = function() {
    
    /**
     * canvas.
     */
    this.canvas = $("#woolmark_canvas")[0];

    this.runnable = true;

    /** 
     * sheep image.
     */
    this.sheep_image = [ new Image(), new Image() ];
    
    /**
     * background image.
     */
    this.background = new Image();

    /**
     * sheep array.
     *
     * <ul>
     *   <li>0: x position.
     *   <li>1: y position.
     *   <li>2: already jumped or not, 0:not jumped, 1:jumped.
     *   <li>3: x jump position.
     * </ul>
     */
    this.sheep_pos = [];

    /**
     * add sheep flag.
     *
     * @type boolean
     */
    this.add_sheep;

    /**
     * action counter, 0 or 1.
     */
    this.action = 0;

    /**
     * sheep number count.
     */
    this.sheep_number = 0;
    
    for(var i = 0; i < 100; i++) {
        this.sheep_pos[i] = [];
        this.sheep_pos[i][0] = 0;
        this.sheep_pos[i][1] = -1;
        this.sheep_pos[i][2] = 0;
        this.sheep_pos[i][3] = 0;
    }

    this.sheep_pos[0][0] = $("#woolmark_canvas").width() - $("#woolmark_canvas").width() % 10 + 10;
    this.sheep_pos[0][1] = 40;
	this.sheep_pos[0][2] = 0;
    this.sheep_pos[0][3] = this.getJumpX(this.sheep_pos[0][1]);

};

/**
 * start animation.
 */
Sheep.prototype.start = function() {
    var self = this;

    this.runnable = true;

    this.background.src = "img/background.gif";
    this.sheep_image[0].src = "img/sheep00.gif";
    this.sheep_image[1].src = "img/sheep01.gif";

    $("#woolmark_canvas").on("vmousedown", function() {
        self.onMouseDown();
    });
    $("#woolmark_canvas").on("vmouseup", function() {
        self.onMouseUp();
    });
    this.run();

};

/**
 * mouse down callback.
 */
Sheep.prototype.onMouseDown = function() {
    this.add_sheep = true;
};

/**
 * mouse up callback.
 */
Sheep.prototype.onMouseUp = function() {
    this.add_sheep = false;
};

/**
 * stop animation.
 */
Sheep.prototype.stop = function() {
    this.runnable = false;
};

/**
 * run as infinite loop.
 */
Sheep.prototype.run = function() {
    var self = this;

    if(!this.canvas || !this.canvas.getContext) {
        return;
    }

    setTimeout(function() {
        self.calc();
        self.draw();

        if(self.runnable) {
            self.run();
        }
    }, 100);
};

/**
 * calculate the sheep position and so on.
 */
Sheep.prototype.calc = function() {

    if(this.add_sheep) {
        this.addSheep();
    }

    for(var i = 0; i < this.sheep_pos.length; i++) {
        if(this.sheep_pos[i][1] >= 0) {
            this.sheep_pos[i][0] -= 5;

            // sheep jumps
            if((this.sheep_pos[i][3] - 20) < this.sheep_pos[i][0] && this.sheep_pos[i][0] <= this.sheep_pos[i][3]) {
                this.sheep_pos[i][1] += 3;
            }

            else {
                if((this.sheep_pos[i][3] - 40) < this.sheep_pos[i][0] && this.sheep_pos[i][0] <= (this.sheep_pos[i][3] - 20)) {
                    this.sheep_pos[i][1] -= 3;
                }
                if(this.sheep_pos[i][0] < (this.sheep_pos[i][3] - 10) && this.sheep_pos[i][2] === 0) {
                    this.sheep_number++;
                    this.sheep_pos[i][2] = 1;
                }
            }

            // remove a frameouted sheep
            if(this.sheep_pos[i][0] < -20) {
                if(i === 0) {
                    this.sheep_pos[0][0] =
                        (this.canvas.width - this.canvas.width % 10) + 10;
                    this.sheep_pos[0][2] = 0;
                    this.sheep_pos[0][3] = this.getJumpX(this.sheep_pos[0][1]);
                } else {
                    this.sheep_pos[i][0] = 0;
                    this.sheep_pos[i][1] = -1;
                    this.sheep_pos[i][2] = 0;
                    this.sheep_pos[i][3] = 0;
                }
            }

        }

    }

    this.action = 1 - this.action;
};

/** 
 * draw the sheep and the background images.
 */
Sheep.prototype.draw = function() {
    var l,
		ctx = this.canvas.getContext("2d"),
		canvas_width = $("#woolmark_canvas").width(),
		canvas_height = $("#woolmark_canvas").height();

    ctx.beginPath();
    ctx.fillStyle = "rgb(120, 255, 120)";
    ctx.fillRect(0, 0, canvas_width, canvas_height);

    ctx.beginPath();
    ctx.fillStyle = "rgb(150, 150, 255)";
    ctx.fillRect(0, 0, canvas_width,
        canvas_height - this.background.height + 10);

    ctx.drawImage(this.background, 40,
        canvas_height - this.background.height);

    for(l = 0; l < this.sheep_pos.length; l++) {
        if(this.sheep_pos[l][1] >= 0) {
            ctx.drawImage(this.sheep_image[this.action],
                this.sheep_pos[l][0],
                canvas_height - this.sheep_pos[l][1]);
        }
    }
    
    ctx.fillStyle = "rgb(0, 0, 0)";
    ctx.textBaseline = "top";
    ctx.fillText("羊が" + this.sheep_number + "匹", 2, 2);
};

/**
 * add new sheep.
 * @private
 */
Sheep.prototype.addSheep = function() {
	var i,
		canvas_width = $("#woolmark_canvas").width(),
		canvas_height = $("#woolmark_canvas").height();

    for(i = 1; i < this.sheep_pos.length; i++) {

        if(this.sheep_pos[i][1] === -1) {
            this.sheep_pos[i][0] = (canvas_width - canvas_width % 10) + 10;
            this.sheep_pos[i][1] = Math.floor(Math.random() * 60) + 10;
            this.sheep_pos[i][2] = 0;
            this.sheep_pos[i][3] = this.getJumpX(this.sheep_pos[i][1]);

            return;

        }

    }

};

/**
 * get jump position.
 * @return x position
 * @private
 */
Sheep.prototype.getJumpX = function(y) {
    return (3 * y / 4 + 40);
};

Sheep.prototype.resize = function() {
    var canvas_width = $("#woolmark_canvas").width(),
        i;

    $("#woolmark_canvas").attr({
        height:$(window).height() -
            $("div.ui-header").outerHeight() -
            $("div.ui-footer").outerHeight() - 4,
        width:$("body").width()});

    if(canvas_width < this.sheep_pos[0][0]) {
        this.sheep_pos[0][0]  = canvas_width + 20;
    }
};

$(function() {
    var sheep = new Sheep();
    sheep.resize();
    sheep.start();

    $(window).resize(function() {
        sheep.resize();
    });

    $("#exit-app").on("vclick", function() {
        if(tizen && tizen.application) {
            tizen.application.getCurrentApplication().exit();
        }
    });
});

